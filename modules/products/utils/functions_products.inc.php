<?php

function validate_products($value){
  //echo json_encode("Inside validate_products on function products inc php");
//  exit;
    $error = array();
    $valid = true;
    $filtro = array(
        'prodname' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp'=>'/^\D{3,20}$/')
        ),
        'prodref' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9A-Za-z]{3,30}$/')
        ),
        'prodprice' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9]{1,30}$/')
        ),
        'proddesc' => array(
            'filter'=>FILTER_VALIDATE_REGEXP,
            'options'=>array('regexp'=>'/^\D{20,300}$/')
        ),
        'date_expiration' => array(
            'filter'=>FILTER_CALLBACK,
            'options'=>'date_expiration'
        ),
        /*'date_expiration' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/')
        ),*/
    );
        $resultado=filter_var_array($value,$filtro);
        $resultado['date_reception'] = $value['date_reception'];
        $resultado['category'] = $value['category'];
        $resultado['packaging'] = $value['packaging'];
        $resultado['commnity'] = $value['commnity'];
        $resultado['province'] = $value['province'];
        $resultado['city'] = $value['city'];

        if(!$resultado['prodname']){
            $error['prodname']='Prodname debe tener de 3 a 30 caracteres';
        }elseif(!$resultado['prodref']){
            $error['prodref']='Prodref debe tener de 3 a 30 caracteres';
        }elseif(!$resultado['prodprice']){
            $error['prodprice']='El precio debe ser entero';
        }elseif(!$resultado['date_expiration']){
            $error['date_expiration']='Debe ser posterior a la de recepcion';
        }elseif(!$resultado['proddesc']){
            $error['proddesc']='Debe tener entre 20 y 500 caracteres';
        }else{
             return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
        };
        return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
}//End of function validate product

/*-----------------------------------------------------------------------*/
function date_expiration($date_expiration) {
    $aux=$_POST['data'];

    if ((strtotime($aux['date_expiration']))>(strtotime($aux['date_reception'])))
        return $date_expiration;
    elseif ((strtotime($aux['date_expiration']))===(strtotime($aux['date_reception'])))
        return $date_expiration;
    else
        return false;
}

