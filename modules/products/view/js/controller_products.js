
//Solution to : "Uncaught Error: Dropzone already attached."
//Dropzone.autoDiscover = false;
$(document).ready(function () {
    //console.log("Inside ready");

    $('#date_expiration').datepicker({
      dateFormat: 'dd-mm-yy', 
      changeMonth: true, 
      changeYear: true, 
      yearRange: '0:+2',
      minDate: 0,
      onSelect: function(selectedDate) {
        }
    });
    $('#date_reception').datepicker({
      dateFormat: 'dd-mm-yy', 
      changeMonth: true, 
      changeYear: true, 
      yearRange: '0:+2',
      minDate: 0,
      onSelect: function(selectedDate) {
        }
    });

    $('#submit_products').click(function(){
        //console.log("Inside click function");
        //console.log($('input[name="packaging"]:checked').val());
        validate_product();
    });

    $.get("../../products/load_data",
          function(response){
            if(response.product===""){
                $("#prodname").val('');
                $("#prodref").val('');
                $("#prodprice").val('');
                $("#date_reception").val('');
                $("#date_expiration").val('');
                $('#error_commnity').val('Select community');
                $('#province').val('Select province');
                $('#city').val('Select city');
                $("#proddesc").val('');
                var inputElements = document.getElementsByClassName('catCheckbox');
                for (var i = 0; i < inputElements.length; i++) {
                    if (inputElements[i].checked){
                        inputElements[i].checked = false;
                    }
                }
            }else{
              $("#prodname").val(response.product.prodname);
              $("#prodref").val(response.product.prodref);
              $("#prodprice").val(response.product.prodprice);
              $("#date_reception").val(response.product.date_reception);
              $("#date_expiration").val(response.product.date_expiration);
              $('#country').val(response.product.country);
              $('#province').val(response.product.province);
              $('#city').val(response.product.city);
              $("#proddesc").val(response.product.proddesc);
              var category = response.product.category;
              var inputElements = document.getElementsByClassName('catCheckbox');
              for (var j = 0; j < category.length; j++) {
                  for (var k = 0; k < inputElements.length; k++) {
                    if (category[j] === inputElements[k]){
                        inputElements[k].checked = true;
                    }
                  }
              }
            }
          }, "json");

    //Dropzone function //////////////////////////////////
    $("#dropzone").dropzone({
        url: "../../products/upload",
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "An error has occurred on the server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                //alert(response);
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
                console.log(file.name);
                console.log("Response: "+response);

                
            });
        },
        complete: function (file) {
            if(file.status == "success"){
            alert("El archivo se ha subido correctamente: " + file.name);
            }
        },
        error: function (file) {
            alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            console.log(name);
            $.ajax({
                type: "POST",
                url: "../../products/delete",
                data: "filename=" + name,
                success: function (data) {
                  //console.log(name);
                  console.log(data);
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(data);
                    //console.log(data);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) !== null) {
                            element.parentNode.removeChild(file.previewElement);
                            //alert("Imagen eliminada: " + name);
                        } else {
                            return false;
                        }
                    } else { //json.res == false, elimino la imagen también
                        var element2;
                        if ((element2 = file.previewElement) !== null) {
                            element2.parentNode.removeChild(file.previewElement);
                        } else {
                            return false;
                        }
                    }

                }
            });
        }
    });//End dropzone

    var string_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
    //var val_dates = /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/;
    var val_dates = /\d{2}.\d{2}.\d{4}$/;
    var prod_ref = /^[0-9a-zA-Z]{2,20}$/;
    var prod_price = /(?=.)^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})?$/;
    var string_description = /^(.){1,500}$/;
    //var string_description = /^[0-9A-Za-z]{2,90}$/;

    /* Fade out function  to hide the error messages */

    $("#prodname").keyup(function () {
      if ($(this).val() !== "" && prod_ref.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#prodref").keyup(function () {
      if ($(this).val() !== "" && prod_ref.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#prodprice").keyup(function () {
      if ($(this).val() !== "" && prod_price.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#proddesc").keyup(function () {
      if ($(this).val() !== "" && string_description.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    //Dependent combos //////////////////////////////////
    load_communities_v1();

    $("#province").append('<option value="" selected="selected">Select province</option>');
    $("#city").append('<option value="" selected="selected">Select city</option>');

    $("#commnity").change(function() {        
      var commnity = document.getElementById('commnity').value;
      //console.log(commnity);
      load_provinces_v1(commnity);
	});

	$("#province").change(function() {
		var prov = document.getElementById('province').value;
		load_cities_v1(prov);
	});

});//End document ready*/

function validate_product(){
    var result = true;

    var prodname = document.getElementById('prodname').value;
    var prodref = document.getElementById('prodref').value;
    var prodprice = document.getElementById('prodprice').value;
    var date_reception = document.getElementById('date_reception').value;
    var date_expiration = document.getElementById('date_expiration').value;
    var category = [];
    var inputElements = document.getElementsByClassName('catCheckbox');
    var proddesc = document.getElementById('proddesc').value;
    var j=0;
    for (var i=0; i< inputElements.length; i++){
        if (inputElements[i].checked){
          category[j] = inputElements[i].value;
          j++;
        }
    }
    var packaging = $('input[name="packaging"]:checked').val();
    var commnity = document.getElementById('commnity').value;
    var province = document.getElementById('province').value;
    var city = document.getElementById('city').value;

    $(".error").remove();

    if (document.formproduct.prodname.value.length===0){
        $("#prodname").focus().after("<span id='e_prodname' class='error'>Tiene que escribir el nombre</span>");
        document.formproduct.prodname.focus();
        return 0;
    }
    
    if (document.formproduct.prodref.value.length===0){
        $("#prodref").focus().after("<span id='e_prodref' class='error'>Tiene que escribir el numero de referencia</span>");
        document.formproduct.prodref.focus();
        return 0;
    }
    
    if (document.formproduct.prodprice.value.length===0){
        $("#prodprice").focus().after("<span id='e_prodprice' class='error'>Tiene que poner un precio</span>");
        document.formproduct.prodprice.focus();
        return 0;
    }

    if (document.formproduct.date_reception.value.length===0){
        $("#date_reception").focus().after("<span id='e_date_reception' class='error'>Tiene que seleccionar una fecha</span>");
        document.formproduct.date_reception.focus();
        return 0;
    }

    if (document.formproduct.date_expiration.value.length===0){
        $("#date_expiration").focus().after("<span id='e_date_expiration' class='error'>Tiene que seleccionar una fecha</span>");
        document.formproduct.date_expiration.focus();
        return 0;
    }

    if (category.length===0){
        $("#error_category").focus().after("<span id='e_category' class='error'>Tiene que seleccionar una categoria</span>");
        document.formproduct.error_category.focus();
        return 0;
    }

    if (document.formproduct.packaging.value.length===0){
        $("#packaging").focus().after("<span id='e_packaging' class='error'>Tiene que seleccionar un tipo de empaquetamiento</span>");
        document.formproduct.packaging.focus();
        return 0;
    }

    if (document.formproduct.proddesc.value.length===0){
        $("#proddesc").focus().after("<span id='e_proddesc' class='error'>Tiene que indicar una descripcion</span>");
        document.formproduct.proddesc.focus();
        return 0;
    }

    /*document.formproduct.submit();
    document.formproduct.action="index.php?page=controller_entrada&op=create";
*/

    if (result){

      var data = {"prodname": prodname, "prodref": prodref, "prodprice": prodprice, "date_expiration": date_expiration, "date_reception": date_reception, "category": category,"packaging": packaging, "commnity": commnity, "province": province, "city": city, "proddesc": proddesc};
      //console.log(data);

      $.post('../../products/alta',
        {data:data}, function (response){
        console.log(response);

        var json = JSON.parse(response);

        if(json.success == 'true'){
          console.log('dentro');
          window.location.href = json.redirect;
        }

        //console.log(response.prodname);
        
        $(".error").remove();
        console.log(json);
        if (json.error.prodname){
            $("#prodname").focus().after("<span id='e_prodname' class='error'>"+json.error.prodname+"</span>");
            document.formproduct.prodname.focus();
            return 0;
        }
        
        if (json.error.prodref){
            $("#prodref").focus().after("<span id='e_prodref' class='error'>"+json.error.prodref+"</span>");
            document.formproduct.prodref.focus();
            return 0;
        }
        
        if (json.error.prodprice){
            $("#prodprice").focus().after("<span id='e_prodprice' class='error'>"+json.error.prodprice+"</span>");
            document.formproduct.prodprice.focus();
            return 0;
        }

        if (json.error.date_expiration){
            $("#date_expiration").focus().after("<span id='e_date_expiration' class='error'>"+json.error.date_expiration+"</span>");
            document.formproduct.date_expiration.focus();
            return 0;
        }

        if (json.error.proddesc){
            $("#proddesc").focus().after("<span id='e_proddesc' class='error'>"+json.error.proddesc+"</span>");
            document.formproduct.proddesc.focus();
            return 0;
        }

        if (json.error_prodpic){
            $("#dropzone").after("<span id='e_dropzone' class='error'>"+json.error_prodpic+"</span>");
            document.formproduct.dropzone.focus();
            return 0;
        }

    });/*,"json").fail(function(xhr, textStatus, errorThrown){
          //console.log(xhr);
          //console.log(textStatus);
          //console.log(errorThrown);
          //console.log(xhr.responseJSON);

          if (xhr.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (xhr.status == 404) {
                alert('Requested page not found [404]');
            } else if (xhr.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + xhr.responseText);
            }
            });//End POST*/
  }//End if result
}//End validate_product
/*
function load_countries_v2(cad) {
    $.getJSON( cad, function(data) {
      $("#country").empty();
      $("#country").append('<option value="" selected="selected">Select country</option>');

      $.each(data, function (i, valor) {
        $("#country").append("<option value='" + valor.sISOCode + "'>" + valor.sName + "</option>");
      });
    })
    .fail(function() {
        alert( "error load_countries" );
    });
}*/

function load_communities_v1() {
  //alert('hola');
    $.post( "../../products/load_communities",
        function( response ) {
          //console.log(response);
          json = JSON.parse(response);

            if(response === 'error'){
                load_countries_v2("resources/ListOfCountryNamesByName.json");
            }else{
              $("#commnity").empty();
              $.each(json, function (i, valor) {
                //console.log(valor.slug);
                //console.log(i);
                $("#commnity").append("<option value="+ valor.slug+ ">" + valor.slug + "</option>");
              });
            var commnity = document.getElementById('commnity').value;
            load_provinces_v1(commnity);

            }
    })
    .fail(function(response) {
        load_countries_v2("resources/ListOfCountryNamesByName.json");
    });
}
/*
function load_provinces_v2() {
    $.get("resources/provinciasypoblaciones.xml", function (xml) {
	    $("#province").empty();
	    $("#province").append('<option value="" selected="selected">Select province</option>');

        $(xml).find("provincia").each(function () {
            var id = $(this).attr('id');
            var name = $(this).find('nombre').text();
            $("#province").append("<option value='" + id + "'>" + name + "</option>");
        });
    })
    .fail(function() {
        alert( "error load_provinces" );
    });
}*/

function load_provinces_v1(community) { //provinciasypoblaciones.xml - xpath
    data = {community : community};
    //console.log(data);
    $.post( "../../products/load_provinces",data,
        function( response ) {
          //console.log(response);
          var json = JSON.parse(response);
          //console.log(json);
        
          if(response === 'error'){
              load_provinces_v2();
          }else{
          $("#province").empty();
            $.each(json, function (i, valor) {
              //console.log(valor.slug);
              //console.log(i);
              $("#province").append("<option value="+ valor.slug+ ">" + valor.slug + "</option>");
            });
            var prov = document.getElementById('province').value;
            load_cities_v1(prov);
          }
    })
    .fail(function(response) {
        //load_provinces_v2();
    });
}
/*
function load_cities_v2(prov) {
    $.get("resources/provinciasypoblaciones.xml", function (xml) {
		$("#city").empty();
	    $("#city").append('<option value="" selected="selected">Select city</option>');

		$(xml).find('provincia[id=' + prov + ']').each(function(){
    		$(this).find('localidad').each(function(){
    			 $("#city").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
    		});
        });
	})
	.fail(function() {
        alert( "error load_cities" );
    });
}*/

function load_cities_v1(prov) { //provinciasypoblaciones.xml - xpath
    var datos = { Provincia : prov  };
    //console.log(datos);
	$.post("../../products/load_cities", datos, function(response) {
  	  //console.log(response);
      var json = JSON.parse(response);
  		var cities=json.cities;
  		//alert(poblaciones);
  		//console.log(poblaciones);
  		//alert(poblaciones[0].poblacion);

  		

          if(response === 'error'){
              load_cities_v2();
          }else{
            $("#city").empty();
            $.each(cities, function (i, valor) {
              //console.log(valor.slug);
              //console.log(i);
              $("#city").append("<option value="+ valor.slug+ ">" + valor.slug + "</option>");
            });
          }
	})
	.fail(function() {
        //load_cities_v2(prov);
  });
  };
