<?php
session_start();

class controller_products {

    function __construct() {

      require_once( UTILS_PRODUCTS . "functions_products.inc.php");
      require_once( UTILS . "upload.php");

    }

    function list_view_cprod(){
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(PRODUCTS_VIEW . 'create_products.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function results_products(){
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(PRODUCTS_VIEW . 'results_products.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function upload() {
        $result_prodpic = upload_files();
        $_SESSION['result_prodpic'] = $result_prodpic;
        //echo json_encode($result_prodpic);
    }

    function delete() {
        $_SESSION['result_prodpic'] = array();
        $result = remove_files();
        if($result === true){
          echo json_encode(array("res" => true));
        }else{
          echo json_encode(array("res" => false));
        }
        //echo json_decode($result);
    }

    function alta() {
      $jsondata = array();
      $producstJSON = $_POST["data"];
      $result= validate_products($producstJSON);

      if (empty($_SESSION['result_prodpic'])){
          $_SESSION['result_prodpic'] = array('result' => true, 'error' => "", "data" => MEDIA_PATH2.'default-avatar.png');
      }

      $result_prodpic = $_SESSION['result_prodpic'];

      if(($result['resultado']==1) && ($result_prodpic['result'])) {
        //echo json_encode($result['datos']['date_reception']);
        //echo json_encode($result_prodpic['data']);

          $arrArgument = array(
            'prodname' => $result['datos']['prodname'],
            'prodref' => $result['datos']['prodref'],
            'prodprice' => $result['datos']['prodprice'],
            'date_reception' => $result['datos']['date_reception'],
            'date_expiration' => $result['datos']['date_expiration'],
            'category' => $result['datos']['category'],
            'packaging' => $result['datos']['packaging'],
            'proddesc' => $result['datos']['proddesc'],
            'commnity' => $result['datos']['commnity'],
            'province' => $result['datos']['province'],
            'city' => $result['datos']['city'],
            'prodpic' => $result_prodpic['data']
          );
          //$arrValue=true;

          $arrValue = "false";

          $arrValue = loadModel(MODEL_PRODUCTS, "products_model", "create_product", $arrArgument);
          //echo json_encode($arrValue);
          //die();
          $_SESSION['result_prodpic']='';
          if ($arrValue){
              $message = "Product has been successfull registered";
          }else{
              $message = "Problem ocurred registering a porduct";
          }

          $_SESSION['product'] = $arrArgument;
          $_SESSION['message'] = $message;
          $callback="../../products/results_products/";

          $jsondata['success'] = "true";
          $jsondata['redirect'] = $callback;
          echo json_encode($jsondata);
          exit;
      }else{
        $jsondata['success'] = false;
        $jsondata['error'] = $result['error'];
        $jsondata['error_prodpic'] = $result_prodpic['error'];

        $jsondata['success1'] = false;
        if ($result_prodpic['result']) {
            $jsondata['success1'] = true;
            $jsondata['prodpic'] = $result_prodpic['data'];
        }
        //header('HTTP/1.0 400 Bad error');

        echo json_encode($jsondata);
      }//End else
    }

    function load_communities(){
      $json = array();

      $json = loadModel(MODEL_PRODUCTS, "products_model", "obtain_communities");
      
      if($json){
        echo json_encode($json);
        exit;
      }else{
        $json = "error";
        echo json_encode($json);
        exit;
      }
    }

    function load_provinces(){
      $community = $_POST['community'];

      $provinces = loadModel(MODEL_PRODUCTS, "products_model", "obtain_provinces", $community);

      if($provinces){
        echo json_encode($provinces);
        exit;
      }else{
        $jsonerror = "error";
        echo json_encode($jsonerror);
        exit;
      }
    }

    function load_cities(){
      $jsondata = array();
      $json = array();

      $json = loadModel(MODEL_PRODUCTS, "products_model", "obtain_cities", $_POST['Provincia']);

      if($json){
        $jsondata["cities"] = $json;
        echo json_encode($jsondata);
        exit;
      }else{
        $jsondata["cities"] = "error";
        echo json_encode($jsondata);
        exit;
      }
    }

    function load_prod(){
      $jsondata = array();
      if (isset($_SESSION['product'])) {
          //echo debug($_SESSION['user']);
          $jsondata["product"] = $_SESSION['product'];
      }
      if (isset($_SESSION['message'])) {
          //echo $_SESSION['msje'];
          $jsondata["message"] = $_SESSION['message'];
      }

      close_session();
      echo json_encode($jsondata);
      exit;
    }

    function close_session() {
      unset($_SESSION['product']);
      unset($_SESSION['message']);
      $_SESSION = array(); // Destruye todas las variables de la sesión
      session_destroy(); // Destruye la sesión
    }

    function load_data(){
          $jsondata = array();

          if (isset($_SESSION['product'])) {
              $jsondata["product"] = $_SESSION['product'];
              echo json_encode($jsondata);
              exit;
          } else {
              $jsondata["product"] = "";
              echo json_encode($jsondata);
              exit;
          }
    }
}