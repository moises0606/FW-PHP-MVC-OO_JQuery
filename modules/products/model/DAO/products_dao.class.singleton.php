<?php
//echo json_encode("products_dao.class.singleton.php");
//exit;

class products_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_product_DAO($db, $arrArgument) {
        $prodname = $arrArgument['prodname'];
        $prodref = $arrArgument['prodref'];
        $prodprice = $arrArgument['prodprice'];
        $date_reception = $arrArgument['date_reception'];
        $date_expiration = $arrArgument['date_expiration'];
        $category = $arrArgument['category'];
        $packaging = $arrArgument['packaging'];
        $commnity = $arrArgument['commnity'];
        $province = $arrArgument['province'];
        $city = $arrArgument['city'];
        $proddesc = $arrArgument['proddesc'];
        $prodpic = $arrArgument['prodpic'];

        $cat1=0;
        $cat2=0;
        $cat3=0;
        $cat4=0;

        foreach ($category as $indice) {
            if ($indice === 'cat1')
                $cat1 = 1;
            if ($indice === 'cat2')
                $cat2 = 1;
            if ($indice === 'cat3')
                $cat3 = 1;
            if ($indice === 'cat4')
                $cat4 = 1;
        }
        
        $sql = "INSERT INTO productos (prodname, prodref, prodprice, date_reception,"
                . " date_expiration, cat1, cat2, cat3, cat4, packaging, commnity, province,"
                . " city, proddesc, prodpic) VALUES ('$prodname', '$prodref',"
                . " '$prodprice', '$date_reception', '$date_expiration', '$cat1',"
                . " '$cat2', '$cat3', '$cat4', '$packaging', '$commnity', '$province',"
                . " '$city', '$proddesc', '$prodpic')";

        return $db->ejecutar($sql);
    }

    public function obtain_communities_DAO($db){
          $sql = "SELECT slug FROM comunidades";

          return $db->listar($sql);
    }

    public function obtain_provinces_DAO($db, $community){
          $sql = "SELECT p.slug FROM comunidades c, provincias p WHERE c.id=p.comunidad_id  AND c.slug='$community'";

          return $db->listar($sql);
    }

    public function obtain_cities_DAO($db, $provincia){
          $sql = "SELECT m.slug FROM municipios m, provincias p WHERE p.id=m.provincia_id AND p.slug='$provincia'";

          return $db->listar($sql);
    }
}//End productDAO
