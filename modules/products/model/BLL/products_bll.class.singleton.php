<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;
/*
$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/';
//define('SITE_ROOT', $path);
define('MODEL_PATH', SITE_ROOT . 'model/');

require(MODEL_PATH . "Db.class.singleton.php");*/
//require(SITE_ROOT . "modules/products/model/DAO/products_dao.class.singleton.php");

class products_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = products_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_product_BLL($arrArgument){
      return $this->dao->create_product_DAO($this->db, $arrArgument);
    }

    public function obtain_communities_BLL(){
      return $this->dao->obtain_communities_DAO($this->db);
    }

    public function obtain_provinces_BLL($arrArgument){
      return $this->dao->obtain_provinces_DAO($this->db, $arrArgument);
    }

    public function obtain_cities_BLL($arrArgument){
      return $this->dao->obtain_cities_DAO($this->db, $arrArgument);
    }
}

