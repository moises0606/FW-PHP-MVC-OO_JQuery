var load_list = function() {
	$.ajax({
        type : 'GET',
        url  : "../../home/list1",
            success: function(json) {
            	//console.log(json);
            	var data = JSON.parse(json);
            	var clave = {clave:data.clave};
            	//console.log(data);
				$divres=$('#hlresults');
				$divres.empty();

				$.ajax({
			        type : 'POST',
			        data: clave,
			        url  : "../../home/list_results",
			            success: function(json) {
			            	//console.log(json);
			            	var entradas = JSON.parse(json);
							//console.log(entradas);
        					
					        $divres.empty();
					        var coordenadas = [];
        					$.each(entradas, function (i, valor) {
									//console.log(valor);
									var aux = {nombre:valor.prodname,latitud:valor.latitud, longitud:valor.longitud};
									//console.log(aux);
									coordenadas.push(aux);
									$divres.append('<div class="hlres" value='+ valor.prodref+'><h3>' + valor.prodname + '</h3>' +
					            	valor.prodprice+"€"+" "+" en la provincia de "+ valor.province + " en "+ valor.city +'</div>');
					       	});
					       	myMap(coordenadas);

        					$("#selection-page").bootpag({
					            total: data.pages,
					            page: 1,
					            maxVisible: 3,
					            next: 'next',
					            prev: 'prev'
					        }).on("page", function (e, num) {
					        	//console.log(num);
					        	$divres.empty();

								var page = {page:num};
								$.ajax({
							        type : 'POST',
							        data: page,
							        url  : "../../home/num_page",
							            success: function(json) {
							            	//console.log(json);
							            	var entradas = JSON.parse(json);
											//console.log(entradas);
											$divres.empty();

											var coordenadas = [];
				        					$.each(entradas, function (i, valor) {
												//console.log(valor);
												var aux = {nombre:valor.prodname,latitud:valor.latitud, longitud:valor.longitud};
												coordenadas.push(aux);

												$divres.append('<div class="hlres" value='+ valor.prodref+'><h3>' + valor.prodname + '</h3>' +
								            	valor.prodprice+"€"+" "+" en la provincia de "+ valor.province + '</div>');
									       	});
									       	//console.log(coordenadas);
					       					myMap(coordenadas);
							            },
							    });

						     });
							
			            },
			    });

            },
    });
}

function myMap(coordenadas) {
    var ubi = {lat: 40.4893538, lng: -3.6827461};

    var map= new google.maps.Map(document.getElementById('map'),{
        center: ubi,
        zoom:6.5,
    });
    $.each(coordenadas, function (i, valor) {
    	//console.log(valor);
    	var lat = valor.latitud;
    	var long = valor.longitud;

		new google.maps.Marker({
	        position:  new google.maps.LatLng(lat,long),
	        map: map,
	        title: valor.nombre
        });
	});
}

$(document).ready(function () {
load_list();

	$(document).on('keyup', "#lentry", function() {
			var entry = document.getElementById('lentry').value;
	        //console.log('hola');
	        
	        $.get("../../home/entry", function (data) {    
	            console.log(data);

	            var json = JSON.parse(data);
	            console.log(json);

	            var entrada=[];
	            $.each(json, function (i, valor) {
	                if (valor.prodname){
	                    entrada.push(valor.prodname);
	                }
	                if (valor.slug){
	                    entrada.push(valor.slug);
	                }
	                //console.log(entrada);
	            });

	            //console.log(entrada);

	            $('#lentry').autocomplete({
	                source: entrada
	            });

	            console.log(data);
	        	
	        	key = {clave:entry};
	        	$.ajax({
	            type : 'POST',
	            data: key,
	            url  : "../../home/savecat",

	        	});
	        });
	        load_list();
	 });

	$(document).on('click', ".hlres", function() {
        var id = this.getAttribute('value');
        /*alert(id);*/
        data = {id:id}
        //console.log(data);

        $.post("../../home/modal", data, function (data, status) {
            console.log(data);
            var json = JSON.parse(data);
            var product = json[0];
            //alert(product.name);



            $("#img_prod").html('<img src="' + product.prodpic + '" height="75" width="75"> ');
            $("#name_prod").html(product.prodname);
            $("#description_prod").html("<strong>Description: <br/></strong>" + product.proddesc);
            $("#price_prod").html("Price: " + product.prodprice + " €");
            $("#date_reception").html("Start date: " + product.date_reception);
            $("#date_expiration").html("Date expiration: " + product.date_expiration);
            $("#lcity").html("Ciudad: " + product.city);


            //we do this so that  details_prod  appear
            $("#details_prod").show();

            $("#modal").dialog({
                width: 850, //<!-- ------------- ancho de la ventana -->
                height: 500, //<!--  ------------- altura de la ventana -->
                //show: "scale", <!-- ----------- animación de la ventana al aparecer -->
                //hide: "scale", <!-- ----------- animación al cerrar la ventana -->
                resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                //position: "down",<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                },
                show: {
                    effect: "blind",
                    duration: 1000
                },
                hide: {
                    effect: "explode",
                    duration: 1000
                }
            });
        });
    });



});