var showcat = 4;

var load_categories = function () {

    $.post("../../home/category",{cant:showcat}, function(data) {
        //console.log(data);
        var json = JSON.parse(data);
    
        $('#hcategory').empty();
        $divcat=$('#hcategory');
        $.each(json, function (i, valor) {
            $divcat.append('<div class="hcat" value=' + valor.slug +'><p>' + valor.slug + '</p></div>');
        });
      });
};


$(document).ready(function () {
load_categories();

    $('#entry').keyup(function(){
        var entry = document.getElementById('entry').value;
        //console.log('hola');
        
        $.get("../../home/entry", function (data) {    
            console.log(data);

            var json = JSON.parse(data);
            console.log(json);

            var entrada=[];
            $.each(json, function (i, valor) {
                if (valor.prodname){
                    entrada.push(valor.prodname);
                }
                if (valor.slug){
                    entrada.push(valor.slug);
                }
                //console.log(entrada);
            });

            //console.log(entrada);

            $('#entry').autocomplete({
                source: entrada
            });

        });
    });

    $(document).on('click', "#hlmbutton", function() {
        showcat=showcat+4;
        load_categories();
    });

    $(document).on('click', "#hbutton", function() {
        var value = document.getElementById('entry').value;
        data = {clave:value};
        //console.log(data);
        $.ajax({
            type : 'POST',
            data: data,
            url  : "../../home/savecat",
                success: function(json) {
                    /*console.log(json);*/
                    var data = JSON.parse(json);
                    if ( data == 'true' ){ //index.php?module=home&view=home_list
                        $callback = "../../home/home_list/";
                        window.location.href=$callback;
                    }else{
                        alert('No hay conexion con el servidor');
                    }
                },
        });
    });

    $(document).on('click', ".hcat", function() {
        var value = this.getAttribute('value');
        data = {clave:value};
        console.log(data);
        $.ajax({
            type : 'POST',
            data: data,
            url  : "../../home/savecat",
                success: function(json) {
                    /*console.log(json);*/
                    var data = JSON.parse(json);
                    if ( data == 'true' ){ //index.php?module=home&view=home_list
                        $callback = "../../home/home_list/";
                        window.location.href=$callback;
                    }else{
                        alert('No hay conexion con el servidor');
                    }
                },
        });

    });

});