<?php
session_start();

class controller_home {

    function __construct() {
        //include(UTILS . "common.inc.php");
    }


    function home() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(MAIN_VIEW . 'home.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function home_list() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(MAIN_VIEW . 'home_list.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function entry(){
        $json = array();
        $json2 = array();
        $data = array();

        try{
        $json = loadModel(MODEL_MAIN, "home_model", "obtain_entries");
        $json2 = loadModel(MODEL_MAIN, "home_model", "obtain_provinces_autocomplete");

        $data = array_merge($json, $json2);

        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($data);
        exit;
    }

    function category(){
        $json = array();

        try{
        $json = loadModel(MODEL_MAIN, "home_model", "obtain_provinces", $_POST['cant']);

        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($json);
        exit;
    }

    function list1(){
        $item_per_page = 4;
        try{
            $total_rows = loadModel(MODEL_MAIN, "home_model", "obtain_pages", $_SESSION['clave']);
            
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }

        if (isset($_SESSION['clave'])){
            $clave = $_SESSION['clave'];
            $datos['clave']=$clave;
            $datos['pages']=ceil($total_rows[0]['total'] / $item_per_page);

            echo json_encode($datos);
        }else{
            echo json_encode('false');
        }
        exit;

    }

    function list_results(){
        $json = array();
        $page_number = 1;
        $item_per_page = 4;

        $position = (($page_number - 1) * $item_per_page);

        $arrArgument = array(
                'position' => $position,
                'item_per_page' => $item_per_page,
                'key' => $_POST['clave']
            );
        try{
            $json = loadModel(MODEL_MAIN, "home_model", "obtain_entries2", $arrArgument);
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($json);
        exit;
    }

    function num_page() {
        $item_per_page = 4;

        if (isset($_POST['page'])){
        
            $result = $_POST["page"];
            if ($result) {
                $page_number = $result;
            }
        }else{
            $page_number = 1;
        }

        if (isset($_SESSION['clave'])){
            $clave = $_SESSION['clave'];
        }else{
            $clave="";
        }
        try {
            $position = (($page_number - 1) * $item_per_page);

            $arrArgument = array(
                'position' => $position,
                'item_per_page' => $item_per_page,
                'key' => $clave
            );

            $arrValue = loadModel(MODEL_MAIN, "home_model", "page_products", $arrArgument);

            echo json_encode($arrValue);
        } catch (Exception $e) {
            echo json_encode("ERROR - 503 BD Unavailable");
        }
        exit;
    }

    function savecat(){
        $_SESSION['clave']=$_POST['clave'];
        if (isset($_SESSION['clave'])){
            echo json_encode('true');
        }else{
            echo json_encode('false');
        }
        exit;
    }
    
    function modal(){
        try{
            $json = loadModel(MODEL_MAIN, "home_model", "lmodal", $_POST['id']);

        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($json);
        exit;
    }
}