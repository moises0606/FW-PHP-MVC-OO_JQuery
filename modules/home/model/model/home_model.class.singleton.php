<?php

class home_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = home_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public Function obtain_entries(){
        return $this->bll->obtain_entries_BLL();
    }

    public Function obtain_entries2($key){
        return $this->bll->obtain_entries2_BLL($key);
    }

    public Function obtain_provinces($cant){
        return $this->bll->obtain_provinces_BLL($cant);
    }

    public Function obtain_provinces_autocomplete(){
        return $this->bll->obtain_provinces_autocomplete_BLL();
    }

    public Function obtain_pages($key){
        return $this->bll->obtain_pages_BLL($key);
    }

    public function page_products($arrArgument) {
        return $this->bll->page_products_BLL($arrArgument);
    }

    public function lmodal($arrArgument) {
        return $this->bll->lmodal_BLL($arrArgument);
    }
}
