<?php

class home_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function obtain_entries_DAO($db){
        $sql = "SELECT prodname, prodref FROM productos";

        return $db->listar($sql);
    }


    public function obtain_entries2_DAO($db, $arrArgument){
        $position = $arrArgument['position'];
        $item_per_page = $arrArgument['item_per_page'];
        $key = $arrArgument['key'];

        $sql = "SELECT prodname,prodprice,province,prodref,city,latitud,longitud FROM productos WHERE province LIKE '%$key%' OR prodname LIKE '%$key%' LIMIT ".$position." , ".$item_per_page;

        return $db->listar($sql);
    }

    public function obtain_provinces_DAO($db, $cant){
        $sql = "SELECT slug FROM provincias ORDER BY population DESC LIMIT $cant";

        return $db->listar($sql);
    }

    public function obtain_provinces_autocomplete_DAO($db){
          $sql = "SELECT slug FROM provincias";

          return $db->listar($sql);
    }

    public function obtain_pages_DAO($db, $key){
        $sql = "SELECT COUNT(*) as total FROM productos WHERE province LIKE '%$key%' OR prodname LIKE '%$key%'";

        return $db->listar($sql);
    }

    public function page_products_DAO($db,$arrArgument) {
        $position = $arrArgument['position'];
        $item_per_page = $arrArgument['item_per_page'];
        $key = $arrArgument['key'];
        
        $sql = "SELECT prodname,prodprice,province,prodref,latitud,longitud  FROM productos WHERE province LIKE '%$key%' OR prodname LIKE '%$key%' LIMIT ".$position." , ".$item_per_page ;

        return $db->listar($sql);
    }

    public function lmodal_DAO($db, $id){
        $sql = "SELECT * FROM productos WHERE prodref='$id'";

        return $db->listar($sql);
    }
}
