<?php

class home_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = home_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function obtain_entries_BLL(){
      return $this->dao->obtain_entries_DAO($this->db);
    }

    public function obtain_entries2_BLL($key){
      return $this->dao->obtain_entries2_DAO($this->db, $key);
    }

    public function obtain_provinces_BLL($cant){
      return $this->dao->obtain_provinces_DAO($this->db, $cant);
    }

    public function obtain_provinces_autocomplete_BLL(){
      return $this->dao->obtain_provinces_autocomplete_DAO($this->db);
    }

    public function obtain_pages_BLL($key){
      return $this->dao->obtain_pages_DAO($this->db, $key);
    }

    public function page_products_BLL($arrArgument) {
        return $this->dao->page_products_DAO($this->db,$arrArgument);
    }

    public function lmodal_BLL($arrArgument) {
        return $this->dao->lmodal_DAO($this->db,$arrArgument);
    }
}
