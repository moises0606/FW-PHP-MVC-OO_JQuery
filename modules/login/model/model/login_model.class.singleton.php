<?php

class login_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = login_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public Function isthereRRSS($token){
        return $this->bll->isthereRRSS_BLL($token);
    }

    public function inputRRSS($user){
        return $this->bll->inputRRSS_BLL($user);
    }
    
    public function loginRRSS($user){
        return $this->bll->loginRRSS_BLL($user);
    }

    public function loginjwt($token){
        return $this->bll->loginjwt_BLL($token);
    }

    public function register($user){
        return $this->bll->register_BLL($user);
    }

    public function activate($token){
        return $this->bll->activate_BLL($token);
    }

    public function loginnormal($user){
        return $this->bll->loginnormal_BLL($user);
    }

    public function update_profile($user){
        return $this->bll->update_profile_BLL($user);
    }

    public function recover_email($user){
        return $this->bll->recover_email_BLL($user);
    }

    public function isthere_email($email){
        return $this->bll->isthere_email_BLL($email);
    }

    public function recover_pass($user){
        return $this->bll->recover_pass_BLL($user);
    }
}
