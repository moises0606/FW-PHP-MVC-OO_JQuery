<?php
session_start();

class controller_login {

    function __construct() {
        require_once(UTILS_LOGIN . "functions_login.inc.php");
        require_once( UTILS . "upload.php");
    }


    function login() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(LOGIN_VIEW . 'login.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function register() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(LOGIN_VIEW . 'register.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function profile_list() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(LOGIN_VIEW . 'profile.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function recover_list() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(LOGIN_VIEW . 'recover.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function recover_pass() {
        $_SESSION['token']=$_GET['token'];
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(LOGIN_VIEW . 'recover_pass.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function profile(){
          $jsondata = array();
          $producstJSON = $_POST;
          $result= validate_profile($producstJSON);



          if($result['resultado']==1) {
            //echo json_encode($result['datos']['date_reception']);
            //echo json_encode($result_prodpic['data']);

            if (!isset($_SESSION['result_prodpic'])){
                $result_avatar = $_SESSION['result_avatar'];
            }else{
                $result_avatar = $_SESSION['result_prodpic'];
            }
            
              $arrArgument = array(
                'name' => $result['datos']['name'],
                'lastname' => $result['datos']['lastname'],
                'dni' => $result['datos']['dni'],
                'date_birthday' => $result['datos']['date_birthday'],
                'commnity' => $result['datos']['commnity'],
                'province' => $result['datos']['province'],
                'city' => $result['datos']['city'],
                'avatar' => $result_avatar['data'],
                'token' => $result['datos']['token']
              );
              //$arrValue=true;

              $arrValue = "false";

              $arrValue = loadModel(MODEL_LOGIN, "login_model", "update_profile", $arrArgument);
              //echo json_encode($arrValue);
              //die();

              if ($arrValue){
                  $message = "Success";
              }else{
                  $message = "error";
              }

              $callback="../../login/profile_list/";

              $jsondata['success'] = "true";
              $jsondata['redirect'] = $callback;
              echo json_encode($jsondata);
              exit;
          }else{
            $jsondata['success'] = false;
            $jsondata['error'] = $result['error'];

            $jsondata['success1'] = false;
            if ($result_avatar['result']) {
                $jsondata['success1'] = true;
                $jsondata['prodpic'] = $result_avatar['data'];
            }
            //header('HTTP/1.0 400 Bad error');

            echo json_encode($jsondata);
        }
    }

    function RRSS(){
        $user = Array();
        $user = $_POST;

        try{
        $json = loadModel(MODEL_LOGIN, "login_model", "isthereRRSS", $user['uid']);

        if ($json[0]['isthere'] == 0){/*Insert in the DDBB*/
            $user['token'] = md5(uniqid(rand(), true)) . md5(uniqid(rand(), true));
            $json2 = loadModel(MODEL_LOGIN, "login_model", "inputRRSS", $user );
            $data['token'] = $user['token'];
            echo json_encode($data);
            exit;
        }else{ /*Is in DDBB, go to login*/
            $user['token'] = md5(uniqid(rand(), true)) . md5(uniqid(rand(), true));
            loadModel(MODEL_LOGIN, "login_model", "loginRRSS", $user);
            $data['token'] = $user['token'];
        }
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($data);
        exit;
    }


    function recover_email(){
        $user = Array();
        $user = $_POST;

        $result= validate_email_recover($user);

        if ($result['resultado']==1){
            try{
            $json = loadModel(MODEL_LOGIN, "login_model", "isthere_email", $user['email']);

            if ($json[0]['isthere'] == 0){
                $user['token'] = md5(uniqid(rand(), true)) . md5(uniqid(rand(), true));

                $json2 = loadModel(MODEL_LOGIN, "login_model", "recover_email", $user );
                
                /*ENVIEAR EMAIL*/
                $result = send_mail_recover_pass($user['email'], $user['token']);

                $data['error']="Success";
            }else{ 
                $data['error']['email'] = "El correo no está registrado";
            }
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
        }else
            $data['error'] = $result['error'];


        echo json_encode($data);
        exit;
    }

    function recover_passBBDD(){
        $user = Array();
        $user = $_POST;
        $user['token']= $_SESSION['token'];
        $_SESSION['token']=' ';

        try{
            $user['password'] = hash("sha256", $user['password']);

            $json = loadModel(MODEL_LOGIN, "login_model", "recover_pass", $user);
            
            $data['error']="Success";
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }

        echo json_encode($data);
        exit;
    }

    function registernormal(){
        $user = Array();
        $user = $_POST;

        if ($user['type']!='login'){
            $result= validate_user($user);
        }
        /*REGISTER*/
        if ($result['resultado']==1){
            try{
            $json = loadModel(MODEL_LOGIN, "login_model", "isthereRRSS", $user['usuario']);

            if ($json[0]['isthere'] == 0){/*Insert in the DDBB*/
                $user['password'] = hash("sha256", $user['password']);
                $user['token'] = md5(uniqid(rand(), true)) . md5(uniqid(rand(), true));
                $user['avatar'] = $grav_url3 = get_gravatar( $user['email'], $s = 80, $d = 'identicon', $r = 'g', $img = false, $atts = array() );

                $json2 = loadModel(MODEL_LOGIN, "login_model", "register", $user );
                /*ENVIEAR EMAIL*/

                $result = send_mail_activation($user['email'], $user['token']);

                $data['error']="Success";
            }else{ 
                $data['error']['usuario'] = "El usuario ya existe";
            }
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
        }else{
            /*LOGIN*/
            if ($user['type']=='login'){
                $json = loadModel(MODEL_LOGIN, "login_model", "isthereRRSS", $user['usuario']);
                if ($json[0]['isthere'] == 1){/*GET USER*/
                    $user['password'] = hash("sha256", $user['password']);
                    $user['token'] = md5(uniqid(rand(), true)) . md5(uniqid(rand(), true));

                    loadModel(MODEL_LOGIN, "login_model", "loginnormal", $user );
                    $json2=loadModel(MODEL_LOGIN, "login_model", "loginjwt", $user['token'] );

                    if ($json2[0]){
                        $data['token']=$user['token'];
                    }else 
                        $data['error']['password'] = "La contrasena no es correcta";
                }else{ 
                    $data['error']['usuario'] = "El usuario desactivado";
                }
            }else
                $data['error'] = $result['error']; /*VALIDACIO PHP REGISTER*/
        }
        echo json_encode($data);
        exit;
    }

    function activate(){
        $user = Array();
        $user = $_GET;

        try{
        $json = loadModel(MODEL_LOGIN, "login_model", "activate", $user['token']);
        
        $callback = '../../login/login/';
        die('<script>window.location.href="'.$callback .'";</script>');

        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }

        exit;
    }

    function loginjwt(){
        $user = Array();
        $user = $_POST;

        try{
        $json = loadModel(MODEL_LOGIN, "login_model", "loginjwt", $user['token']);
        $_SESSION['result_avatar']['data']=$json[0]['avatar'];

        if ($json[0]){
            echo json_encode($json[0]);
        }else{ 
            echo json_encode('error');
        }
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        
        exit;
    }
}

