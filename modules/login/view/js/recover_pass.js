$(document).ready(function () {

	$(document).on('click', "#recoverpassb", function() {
			var password = document.getElementById('passw').value;
			var password1 = document.getElementById('passw1').value;

		    $(".error").remove();

		    if (document.recoverpassform.passw.value.length===0){
		        $("#passw").focus().after("<span id='e_email' class='error'>Tiene que la nueva contraseña</span>");
		        document.recoverpassform.passw.focus();
		        return 0;
		    }

		    if (document.recoverpassform.passw1.value.length===0){
		        $("#passw1").focus().after("<span id='e_email' class='error'>Repita la contraseña nueva</span>");
		        document.recoverpassform.passw1.focus();
		        return 0;
		    }

		    if (password != password1){
		        $("#passw1").focus().after("<span id='e_email' class='error'>La contraseña no coincide</span>");
		        document.recoverpassform.passw1.focus();
		        return 0;
		    }

		    var data = {"password": password};

	        $.ajax({
	            type : 'POST',
	            data: data,
	            url  : "../../login/recover_passBBDD",
	                success: function(json) {
	                    console.log(json);
	                    json = JSON.parse(json);
	                    /*console.log(user);*/
	                    if (json.error == "Success"){
	                    	toastr.success('Contraseña actualizada');
	                    	setTimeout(function() { 
	                    		$callback = "../../login/login/";
	                        	window.location.href=$callback;
	                    	}, 1000);
	                    	
	                    }
	                },
	        });
		});



});