function eliminarDiacriticos(texto) {
    return texto.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
}

function DeleteTokenLocalStorage() {
    var ltoken;
    if (window.localStorage){
        //console.log(ltoken);
        localStorage.ltoken = JSON.stringify(ltoken);
    }
}

$(document).ready(function () {
    //DeleteTokenLocalStorage();
    $('#muser').css("visibility", "hidden");
    //$('#mloutbutton').css("display", "none");

    if ((localStorage.ltoken) && (localStorage.ltoken!="undefined")){
        $('#muser').css("visibility", "visible");
        token = JSON.parse(localStorage.ltoken);
        data = {token:token[0]};
        //console.log(data);

        var logoutbutton = "<li><a id='mloutbutton'>LOGOUT</a></li>";
        $('#menu').append(logoutbutton);

        $.ajax({
            type : 'POST',
            data: data,
            url  : "../../login/loginjwt",
                success: function(json) {
                    /*console.log(json);*/
                    user = JSON.parse(json);
                    /*console.log(user);*/
                    if (user=='error'){
                        toastr.error('Usuario desconectado');
                        DeleteTokenLocalStorage();
                        setTimeout(function() { 
                            $callback = "../../login/login/";
                            window.location.href=$callback;
                        }, 1000);
                    }

                    if (user.activado==0){
                        toastr.error('Usuario desactivado');
                        DeleteTokenLocalStorage();
                        setTimeout(function() { 
                            $callback = "../../login/login/";
                            window.location.href=$callback;
                        }, 1000);
                    }

                    if (user.tipo!='admin'){
                        $('#mlbutton').css("display", "none"); 
                        $('#mproducts').css("display", "none");
                        var row =  "<img style='margin-top: -10px' width='30px' height='30px' src="+user.avatar +"></img>";
                        $('#muser').html(row);
                    }
                     
                },
        });

        
    }else
        $('#mproducts').css("display", "none");


    $(document).on('click', "#mloutbutton", function() {
        DeleteTokenLocalStorage();
        $callback = "../../login/login/";
        window.location.href=$callback;
    });
});