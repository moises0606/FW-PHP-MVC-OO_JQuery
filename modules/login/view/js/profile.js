var validate_profile = function () {
		var name = document.getElementById('name').value;
	    var lastname = document.getElementById('lastname').value;
	    var dni = document.getElementById('dni').value;
	    var date_birthday = document.getElementById('dni').value;

	    $(".error").remove();

	    if (document.form_user_profile.name.value.length===0){
	        $("#name").focus().after("<span id='e_name' class='error'>Tiene que escribir el usuario</span>");
	        document.form_user_profile.name.focus();
	        return 0;
	    }
	    
	    if (document.form_user_profile.lastname.value.length===0){
	        $("#lastname").focus().after("<span id='e_lastname' class='error'>Tiene que escribir email</span>");
	        document.form_user_profile.lastname.focus();
	        return 0;
	    }
	    
	    if (document.form_user_profile.dni.value.length===0){
	        $("#dni").focus().after("<span id='e_dni' class='error'>Tiene que poner una contraseña</span>");
	        document.form_user_profile.dni.focus();
	        return 0;
	    }

	   	if (document.form_user_profile.date_birthday.value.length===0){
	        $("#date_birthday").focus().after("<span id='e_date_birthday' class='error'>Tiene que poner una fecha</span>");
	        document.form_user_profile.date_birthday.focus();
	        return 0;
	    }
};

$(document).ready(function () {
	$('#date_birthday').datepicker({
		dateFormat: 'dd-mm-yy', 
		changeMonth: true, 
		changeYear: true, 
		yearRange: '1950:2000',
		onSelect: function(selectedDate) {
		}
	});

	$.ajax({
            type : 'POST',
            data: data,
            url  : "../../login/loginjwt",
                success: function(json) {
                    /*console.log(json);*/
                    user = JSON.parse(json);
                    /*console.log(user);*/
                    if (user.nombre){
                    	document.getElementById('name').value=user.nombre;
                    }
                    if (user.apellidos){
                    	document.getElementById('lastname').value=user.apellidos;

                    }
                  	if (user.dni){
                  		document.getElementById('dni').value=user.dni;

                  	}
               		if (user.date_birthday){
               			document.getElementById('date_birthday').value=user.date_birthday;

               		}

                },
        });

	$(document).on('click', "#update_products", function() {
		validate_profile();
		var name = document.getElementById('name').value;
	    var lastname = document.getElementById('lastname').value;
	    var dni = document.getElementById('dni').value;
	    var date_birthday = document.getElementById('date_birthday').value;
	    var commnity = document.getElementById('commnity').value;
	    var province = document.getElementById('province').value;
	    var city = document.getElementById('city').value;

      	var data = {'token':user.token, 'name':name,'lastname':lastname,'dni':dni,'date_birthday':date_birthday,'commnity':commnity,'province':province, 'city':city};

      	//console.log(data);

		$.ajax({
            type : 'POST',
            data: data,
            url  : "../../login/profile",
                success: function(json) {
                    console.log(json);
                    user = JSON.parse(json);
                    /*console.log(user);*/
                    
                    if (user.success='true'){
                    	toastr.success('Perfil actualizado');
                    	setTimeout(function() { 
                    		$callback = user.redirect;
                        	window.location.href=$callback;
                    	}, 1000);
                    }
                    if (user.error){
                    	$(".error").remove();

                    	if (user.error.name){
						        $("#name").focus().after("<span id='e_name' class='error'>" + user.error.name+ "</span>");
						        document.register_user.name.focus();
					    }

					    if (user.error.lastname){
						        $("#lastname").focus().after("<span id='e_lastname' class='error'>"+ user.error.lastname+"</span>");
						        document.register_user.lastname.focus();
						}
					    
					    if (user.error.dni){
						        $("#dni").focus().after("<span id='e_dni' class='error'>"+ user.error.dni+"</span>");
						        document.register_user.dni.focus();
						}

						if (user.error.date_birthday){
						        $("#date_birthday").focus().after("<span id='e_date_birthday' class='error'>"+ user.error.date_birthday+"</span>");
						        document.register_user.date_birthday.focus();
						}
                    }

                },
        });
	});
});