var ltoken = [];
var login = function ($login) {
    const promise = new Promise(function(resolve, reject){
        var authService = firebase.auth();
        
        if ($login == "facebook")
            var provider = new firebase.auth.FacebookAuthProvider();
        if ($login == "twitter")
            var provider = new firebase.auth.TwitterAuthProvider();
        if ($login == "google")
            var provider = new firebase.auth.GoogleAuthProvider();

        authService.signInWithPopup(provider)
        .then(function(result) {
            resolve(result);
        })
        .catch(function(error) {
            console.log('Detectado un error:', error);
        });
    });
    return promise;
};

function tokenToLocalStorage() {
    if (window.localStorage){
        //console.log(ltoken);
        localStorage.ltoken = JSON.stringify(ltoken);
    }
}

$(document).ready(function () {
    var config = {
          apiKey: "AIzaSyB5jqWS6H4z1ldKXc25pcyMQNm9-SMT5dQ",
          authDomain: "fwphpmvcoojquery.firebaseapp.com",
          databaseURL: "https://fwphpmvcoojquery.firebaseio.com",
          projectId: "fwphpmvcoojquery",
          storageBucket: "fwphpmvcoojquery.appspot.com",
          messagingSenderId: "241847401702"
        };
        firebase.initializeApp(config);

    $(document).on('click', "#fblogin", function() {
        login('facebook').then(function(result){
            var email, name, lastname, avatar, uid, user;
            //console.log(result);

            user = eliminarDiacriticos(result.additionalUserInfo.profile.first_name);
            email = result.additionalUserInfo.profile.email;
            name = eliminarDiacriticos(result.additionalUserInfo.profile.first_name);
            lastname = eliminarDiacriticos(result.additionalUserInfo.profile.last_name);
            avatar = result.user.photoURL;
            uid = result.additionalUserInfo.profile.id;

            data = {name:name, lastname:lastname, email:email, avatar:avatar, uid:uid, user:user};
            //console.log(data);

            $.ajax({
            type : 'POST',
            data: data,
            url  : "../../login/RRSS",
                success: function(json) {
                    //console.log(json);
                    json = JSON.parse(json);
                    aux = json.token;
                    ltoken.push(aux);
                    tokenToLocalStorage();
                    $callback = "../../home/home/"; /*Cambiar al home*/
                    window.location.href=$callback;
                },
            });

        });

 
    });

    $(document).on('click', "#twlogin", function() {
        login('twitter').then(function(result){
            var email, name, lastname, avatar, uid, user;
            //console.log(result);

            user = eliminarDiacriticos(result.additionalUserInfo.username);
            email = result.additionalUserInfo.profile.email;
            name = eliminarDiacriticos(result.additionalUserInfo.profile.name);
            lastname = eliminarDiacriticos(result.additionalUserInfo.profile.name);
            avatar = result.user.photoURL;
            uid = result.additionalUserInfo.profile.id_str;

            data = {name:name, lastname:lastname, email:email, avatar:avatar, uid:uid, user:user};
            //console.log(data);

            $.ajax({
            type : 'POST',
            data: data,
            url  : "../../login/RRSS",
                success: function(json) {
                    //console.log(json);
                    json = JSON.parse(json);
                    aux = json.token;
                    ltoken.push(aux);
                    tokenToLocalStorage();
                    $callback = "../../home/home/"; /*Cambiar al home*/
                    window.location.href=$callback;
                },
            });
        });
    });

    $(document).on('click', "#glogin", function() {
        login('google').then(function(result){
            var email, name, lastname, avatar, uid, user;
            //console.log(result);

            user = eliminarDiacriticos(result.user.displayName);
            email = result.additionalUserInfo.profile.email;
            name = eliminarDiacriticos(result.additionalUserInfo.profile.given_name);
            lastname = eliminarDiacriticos(result.additionalUserInfo.profile.family_name);
            avatar = result.user.photoURL;
            uid = result.additionalUserInfo.profile.id;

            data = {name:name, lastname:lastname, email:email, avatar:avatar, uid:uid, user:user};
            //console.log(data);

            $.ajax({
            type : 'POST',
            data: data,
            url  : "../../login/RRSS",
                success: function(json) {
                    //console.log(json);
                    json = JSON.parse(json);
                    aux = json.token;
                    ltoken.push(aux);
                    tokenToLocalStorage();
                    $callback = "../../home/home/"; /*Cambiar al home*/
                    window.location.href=$callback;
                },
            });
        });
    });

    $(document).on('click', "#loginb", function() {
        var usuario = document.getElementById('usuario').value;
        var password = document.getElementById('passw').value;

        $(".error").remove();

        if (document.formlogin.usuario.value.length===0){
            $("#usuario").focus().after("<span id='e_rusuario' class='error'>Tiene que escribir el usuario</span>");
            document.formlogin.usuario.focus();
            return 0;
        }
        
        if (document.formlogin.passw.value.length===0){
            $("#passw").focus().after("<span id='e_passw' class='error'>Tiene que poner una contraseña</span>");
            document.formlogin.passw.focus();
            return 0;
        }

        var user = {'type':'login',"usuario": usuario, "password": password};

        $.ajax({
            type : 'POST',
            data: user,
            url  : "../../login/registernormal",
                success: function(json) {
                    console.log(json);
                    user = JSON.parse(json);
                    /*console.log(user);*/

                    if (user.token){
                        aux = user.token;
                        ltoken.push(aux);
                        tokenToLocalStorage();
                        $callback = "../../home/home/"; /*Cambiar al home*/
                        window.location.href=$callback;
                    }

                    if (user.error){
                        $(".error").remove();

                        if (user.error.usuario){
                                $("#usuario").focus().after("<span id='e_usuario' class='error'>" + user.error.usuario+ "</span>");
                                document.register_user.usuario.focus();
                        }
                    
                    if (user.error.password){
                                $("#passw").focus().after("<span id='e_passw' class='error'>" + user.error.password+ "</span>");
                                document.register_user.passw.focus();
                        }    
                    }
                },
        });
    });
});