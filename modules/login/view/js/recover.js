$(document).ready(function () {

	$(document).on('click', "#recoverb", function() {
			var email = document.getElementById('email').value;

		    $(".error").remove();

		    if (document.recoverform.email.value.length===0){
		        $("#email").focus().after("<span id='e_email' class='error'>Tiene que escribir el email</span>");
		        document.recoverform.email.focus();
		        return 0;
		    }

		    var user = {"email": email};

	        $.ajax({
	            type : 'POST',
	            data: user,
	            url  : "../../login/recover_email",
	                success: function(json) {
	                    console.log(json);
	                    user = JSON.parse(json);
	                    /*console.log(user);*/
	                    if (user.error == "Success"){
	                    	toastr.success('Revisa el correo');
	                    	setTimeout(function() { 
	                    		$callback = "../../login/login/";
	                        	window.location.href=$callback;
	                    	}, 1000);
	                    	
	                    }
	                    if (user.error){
	                    	$(".error").remove();

	                    	if (user.error.email){
							        $("#email").focus().after("<span id='e_email' class='error'>" + user.error.email+ "</span>");
							        document.recoverform.email.focus();
						    }
	                    }
	                },
	        });
		});



});