$(document).ready(function () {

	$(document).on('click', "#registerb", function() {
		var usuario = document.getElementById('rusuario').value;
	    var email = document.getElementById('remail').value;
	    var password = document.getElementById('passw1').value;

	    $(".error").remove();

	    if (document.register_user.rusuario.value.length===0){
	        $("#rusuario").focus().after("<span id='e_rusuario' class='error'>Tiene que escribir el usuario</span>");
	        document.register_user.rusuario.focus();
	        return 0;
	    }
	    
	    if (document.register_user.remail.value.length===0){
	        $("#remail").focus().after("<span id='e_remail' class='error'>Tiene que escribir email</span>");
	        document.register_user.remail.focus();
	        return 0;
	    }
	    
	    if (document.register_user.passw1.value.length===0){
	        $("#passw1").focus().after("<span id='e_passw1' class='error'>Tiene que poner una contraseña</span>");
	        document.register_user.passw1.focus();
	        return 0;
	    }

	    var user = {"usuario": usuario, "email": email, "password": password};

        $.ajax({
            type : 'POST',
            data: user,
            url  : "../../login/registernormal",
                success: function(json) {
                    console.log(json);
                    user = JSON.parse(json);
                    /*console.log(user);*/
                    if (user.error == "Success"){
                    	toastr.success('Revisa el correo para activacion');
                    	setTimeout(function() { 
                    		$callback = "../../login/login/";
                        	window.location.href=$callback;
                    	}, 1000);
                    	
                    }
                    if (user.error){
                    	$(".error").remove();

                    	if (user.error.usuario){
						        $("#rusuario").focus().after("<span id='e_rusuario' class='error'>" + user.error.usuario+ "</span>");
						        document.register_user.rusuario.focus();
					    }

					    if (user.error.email){
						        $("#remail").focus().after("<span id='e_remail' class='error'>"+ user.error.email+"</span>");
						        document.register_user.remail.focus();
						}
					    
					    if (user.error.password){
						        $("#passw1").focus().after("<span id='e_passw1' class='error'>"+ user.error.password+"</span>");
						        document.register_user.passw1.focus();
						}
                    }
                },
        });
	});


});