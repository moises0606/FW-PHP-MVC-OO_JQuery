-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: entradas_v2
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `prodname` varchar(20) NOT NULL,
  `prodref` varchar(30) NOT NULL,
  `prodprice` int(11) NOT NULL,
  `date_reception` varchar(30) NOT NULL,
  `date_expiration` varchar(30) NOT NULL,
  `cat1` varchar(45) DEFAULT NULL,
  `cat2` varchar(45) DEFAULT NULL,
  `cat3` varchar(45) DEFAULT NULL,
  `cat4` varchar(45) DEFAULT NULL,
  `packaging` varchar(45) NOT NULL,
  `commnity` varchar(45) NOT NULL,
  `province` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `proddesc` varchar(500) NOT NULL,
  `prodpic` varchar(300) NOT NULL,
  `latitud` varchar(500) DEFAULT NULL,
  `longitud` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`prodref`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES ('testsss','15cad151d2',10,'22-04-2018','22-04-2018','1','0','0','0','none','cataluna','barcelona','barcelona','sssssssssaceeeeeeeeeeeeeeeeeeesssssscrargeggrrgaggragrgrrggrarag','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/1333623820-imagen.png','41.3850639','2.1734035'),('prueba','15db16df15',10,'22-04-2018','22-04-2018','1','0','0','0','none','cataluna','barcelona','barcelona','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/2028632001-imagen.png','41.3950639','2.1934035'),('memsms','2da22d',20,'22-04-2018','22-04-2018','1','0','0','0','none','cataluna','barcelona','barcelona','sakesdkmsvdkmlslkmdskmlsdmkmkldmkskmlsdkmldkmlskmsdmkms','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/1303062423-imagen.png','41.3050639','2.1334035'),('moises','5164rvs561',10,'23-04-2018','23-04-2018','1','0','0','0','none','cataluna','barcelona','barcelona','ravaknjlasjnkjnkscajnkddsnjkdjnkdsjnknjksnjdknkdkdsjdsnknsdnjdsnjdn','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png','41.3650639','2.1034035'),('aaa','aass',4,'17-04-2018','19-04-2018','0','1','0','0','none','andalucia','almeria','abla','sacccccccccccccccccccsacccccccccccccccccccsacccccccccccccccccccsacccccccccccccccccccsaccccccccccccccccccc','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png',NULL,NULL),('aaa','aassa',4,'17-04-2018','19-04-2018','0','0','1','0','none','andalucia','almeria','abla','sacccccccccccccccccccsacccccccccccccccccccsacccccccccccccccccccsacccccccccccccccccccsaccccccccccccccccccc','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png',NULL,NULL),('aaa','aassaa',4,'17-04-2018','19-04-2018','0','0','1','0','none','madrid','madrid','abla','sacccccccccccccccccccsacccccccccccccccccccsacccccccccccccccccccsacccccccccccccccccccsaccccccccccccccccccc','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png',NULL,NULL),('aaa','aassaa4',4,'17-04-2018','19-04-2018','0','1','0','0','none','cataluna','barcelona','abrera','sacccccccccccccccccccsacccccccccccccccccccsacccccccccccccccccccsacccccccccccccccccccsaccccccccccccccccccc','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png','41.5160856','1.9021424'),('test','s4842',6,'18-04-2018','20-04-2018','0','0','1','0','none','cataluna','barcelona','barcelona','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png','41.3650639','2.1624035'),('cualquiera','s4sfv',20,'28-04-2018','29-04-2018','0','1','0','0','none','cataluna','barcelona','barcelona','jsjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png','41.3080639','2.1934035'),('Prueba','ss5fhg',5,'20-04-2018','28-04-2018','0','1','0','0','none','cataluna','barcelona','barcelona','ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png','41.3170639','2.1734035'),('ssssssss','sss',7,'18-04-2018','27-04-2018','0','0','0','1','none','cataluna','barcelona','abrera','ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png','41.5360856','1.9081424'),('ssssssss','sss2',7,'18-04-2018','27-04-2018','0','0','1','0','none','cataluna','barcelona','abrera','ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png','41.5260856','1.9621424'),('sssaa','sss55',6,'11-04-2018','19-04-2018','0','1','0','0','none','madrid','madrid','abla','sssssssssssssssssssssssssssssssssssssssssssssssssssssfffffffffffffffffffffff','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png',NULL,NULL),('sss','sss8s8',6,'18-04-2018','26-04-2018','0','1','0','0','none','madrid','madrid','abla','sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss','/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/media/default-avatar.png',NULL,NULL);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-15 16:38:55
