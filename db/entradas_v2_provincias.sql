-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: entradas_v2
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provincias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `provincia` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `comunidad_id` int(10) unsigned NOT NULL,
  `capital_id` int(11) NOT NULL DEFAULT '-1',
  `population` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_provincia` (`provincia`),
  KEY `FK_provincias` (`comunidad_id`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provincias`
--

LOCK TABLES `provincias` WRITE;
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` VALUES (1,'alava','Álava',16,46,NULL),(2,'albacete','Albacete',8,54,NULL),(3,'alicante','Alicante',10,152,NULL),(4,'almeria','Almería',1,292,NULL),(5,'vila','Ávila',7,395,NULL),(6,'badajoz','Badajoz',11,644,NULL),(7,'illes-balears','Illes Balears',4,836,NULL),(8,'barcelona','Barcelona',9,881,6),(9,'burgos','Burgos',7,1220,NULL),(10,'caceres','Cáceres',11,1580,NULL),(11,'cadiz','Cádiz',1,1776,NULL),(12,'castellon','Castellón',10,1844,NULL),(13,'ciudad-real','Ciudad Real',8,1978,NULL),(14,'cordoba','Córdoba',1,2065,4),(15,'a-coruna','A Coruña',12,2150,NULL),(16,'cuenca','Cuenca',8,2285,NULL),(17,'girona','Girona',9,2526,NULL),(18,'granada','Granada',1,2747,4),(19,'guadalajara','Guadalajara',8,2947,NULL),(20,'guipuzcoa','Guipúzcoa',16,3159,NULL),(21,'huelva','Huelva',1,3257,NULL),(22,'huesca','Huesca',2,3396,NULL),(23,'jaen','Jaén',1,3545,NULL),(24,'leon','León',7,3676,NULL),(25,'lleida','Lleida',9,3918,NULL),(26,'la-rioja','La Rioja',17,4124,NULL),(27,'lugo','Lugo',12,4238,NULL),(28,'madrid','Madrid',13,4356,5),(29,'malaga','Málaga',1,4523,NULL),(30,'murcia','Murcia',14,4588,NULL),(31,'navarra','Navarra',15,4815,NULL),(32,'ourense','Ourense',12,4925,NULL),(33,'asturias','Asturias',3,5009,NULL),(34,'palencia','Palencia',7,5137,NULL),(35,'las-palmas','Las Palmas',5,5252,NULL),(36,'pontevedra','Pontevedra',12,5312,NULL),(37,'salamanca','Salamanca',7,5588,NULL),(38,'santa-cruz-de-tenerife','Santa Cruz de Tenerife',5,5732,NULL),(39,'cantabria','Cantabria',6,5823,NULL),(40,'segovia','Segovia',7,6024,NULL),(41,'sevilla','Sevilla',1,6152,4),(42,'soria','Soria',7,6307,NULL),(43,'tarragona','Tarragona',9,6499,NULL),(44,'teruel','Teruel',2,6721,NULL),(45,'toledo','Toledo',8,6934,NULL),(46,'valencia','Valencia',10,7219,4),(47,'valladolid','Valladolid',7,7415,NULL),(48,'vizcaya','Vizcaya',16,7489,NULL),(49,'zamora','Zamora',7,7821,NULL),(50,'zaragoza','Zaragoza',2,8113,NULL),(51,'ceuta','Ceuta',18,8115,NULL),(52,'melilla','Melilla',19,8116,NULL);
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-15 16:38:56
