<?php

//SITE_ROOT
$path2 = '/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/';

$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/';
define('SITE_ROOT', $path);

//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/programacio/workspace_moises/FW_PHP-AngularJS/1_Backend/5_dependent_dropdowns/');

define('HOME', SITE_PATH . 'home/home/');

//JS
define('JS_PATH', SITE_PATH . 'view/js');

//IMG
define('IMG_PATH', SITE_PATH . 'view/img/');

//CSS
define('CSS_PATH', SITE_PATH . 'view/css');

//PLUGIN
define('PLUGIN_PATH', SITE_PATH . 'view/plugins');

define('PRODUCTION', true);

//model
define('MODEL_PATH', SITE_ROOT . 'model/');
//view
define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');
define('VIEW_PATH_INC_ERROR', SITE_ROOT . 'view/inc/templates_error/');
//modules
define('MODULES_PATH', SITE_ROOT . 'modules/');
//resources
define('RESOURCES', SITE_ROOT . 'resources/');
//media
define('MEDIA_PATH', SITE_ROOT . 'media/');
define('MEDIA_PATH2', $path2 . 'media/');

//utils
define('UTILS', SITE_ROOT . 'utils/');

//model Home
define('UTILS_MAIN', SITE_ROOT . 'modules/home/utils/');
define('MAIN_JS_LIB_PATH', SITE_PATH . 'modules/home/view/lib/');
define('MAIN_JS_PATH', SITE_PATH . 'modules/home/view/js/');
define('MODEL_PATH_MAIN', SITE_ROOT . 'modules/home/model/');
define('MAIN_VIEW', 'modules/home/view/');
define('DAO_MAIN', SITE_ROOT . 'modules/home/model/DAO/');
define('BLL_MAIN', SITE_ROOT . 'modules/home/model/BLL/');
define('MODEL_MAIN', SITE_ROOT . 'modules/home/model/model/');

//model Products
define('UTILS_PRODUCTS', SITE_ROOT . 'modules/products/utils/');
define('PRODUCTS_JS_LIB_PATH', SITE_PATH . 'modules/products/view/lib/');
define('PRODUCTS_JS_PATH', SITE_PATH . 'modules/products/view/js/');
define('MODEL_PATH_PRODUCTS', SITE_ROOT . 'modules/products/model/');
define('PRODUCTS_VIEW', 'modules/products/view/');
define('DAO_PRODUCTS', SITE_ROOT . 'modules/products/model/DAO/');
define('BLL_PRODUCTS', SITE_ROOT . 'modules/products/model/BLL/');
define('MODEL_PRODUCTS', SITE_ROOT . 'modules/products/model/model/');

//model Contact
define('UTILS_CONTACT', SITE_ROOT . 'modules/contact/utils/');
define('CONTACT_JS_LIB_PATH', SITE_PATH . 'modules/contact/view/lib/');
define('CONTACT_JS_PATH', SITE_PATH . 'modules/contact/view/js/');
define('MODEL_PATH_CONTACT', SITE_ROOT . 'modules/contact/model/');
define('CONTACT_VIEW', 'modules/contact/view/');
define('DAO_CONTACT', SITE_ROOT . 'modules/contact/model/DAO/');
define('BLL_CONTACT', SITE_ROOT . 'modules/contact/model/BLL/');
define('MODEL_CONTACT', SITE_ROOT . 'modules/contact/model/model/');

//model Login
define('UTILS_LOGIN', SITE_ROOT . 'modules/login/utils/');
define('LOGIN_JS_LIB_PATH', SITE_PATH . 'modules/login/view/lib/');
define('LOGIN_JS_PATH', SITE_PATH . 'modules/login/view/js/');
define('MODEL_PATH_LOGIN', SITE_ROOT . 'modules/login/model/');
define('LOGIN_VIEW', 'modules/login/view/');
define('DAO_LOGIN', SITE_ROOT . 'modules/login/model/DAO/');
define('BLL_LOGIN', SITE_ROOT . 'modules/login/model/BLL/');
define('MODEL_LOGIN', SITE_ROOT . 'modules/login/model/model/');

//amigables
define('URL_AMIGABLES', TRUE);

